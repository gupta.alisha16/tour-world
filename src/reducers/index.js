import { combineReducers } from 'redux'
import fav from './fav';

 const appReducer = combineReducers({
  fav: fav
})

function rootReducer (state, action) {
  if (action.type === 'CLEAR_STORE') {
    state = undefined
  }

  return appReducer(state, action)
}

export default rootReducer;
