const fav = {
    favPlaces : [],
    //isFav : "0"
}

function favorite (state=fav, action) {
    switch(action.type) {
        case 'PUSH_FAV' : 
            var index = state.favPlaces.findIndex(el => el.id === action.value.id);
            var tempArr = state.favPlaces;
            action.value.isFav = "1";
            if(index<0) {
            tempArr.push(action.value);
            }
            return Object.assign({}, state, {
                favPlaces : tempArr
            })

        case 'REMOVE_FAV' : 
            let quesIndex = state.favPlaces.findIndex( el => el.id === action.value.id)
            action.value.isFav = "0";
            if (quesIndex > -1)
                return Object.assign({}, state, {
                    favPlaces: state.favPlaces.filter((item, index) => index !== quesIndex)
                })
            return state

        default:
        return state;
    }
}

export default favorite;
