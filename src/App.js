import React, { Component } from 'react';
import './App.css';
import NavBar from './containers/navbar'

class App extends Component {
  render() {
    return (
      <div className="demo-big-content">
    <NavBar history = {this.props.history}/>
      
</div>
    );
  }
}

export default App;
