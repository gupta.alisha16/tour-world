import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import { BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore , applyMiddleware,compose} from 'redux';
import { createBrowserHistory } from 'history';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk'
import rootReducer from './reducers';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const history = createBrowserHistory()

const middleware = [thunk];
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}
export const store = createStore(
  // connectRouter(history),
  // composeEnhancer(
  //   applyMiddleware(
  //     routerMiddleware(history),
  //   ),
  // ),
  rootReducer
)

ReactDOM.render(
  <Provider store={store}>
  <BrowserRouter>
  <App history={history}/>
  </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
