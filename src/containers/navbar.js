import React from 'react';
import {Layout, Header, Navigation, Drawer, Content} from 'react-mdl';
import Main from './main';
import './../../src/App.css';
import { withRouter } from 'react-router-dom';


export class NavBar extends React.PureComponent {
    render() {
        return (
            <div className="demo-big-content">
    <Layout>
        <Header className="header-color" title="Tour The World" scroll>
            
        </Header>
        <Drawer title="Tour The World">
            <Navigation>
                <a onClick={()=>{this.props.history.push('/')}} >Home</a>
                <a onClick={()=>{this.props.history.push('/favorite')}}>Favorites</a>
            </Navigation>
        </Drawer>
        <Content>
            <div className="page-content" />
            <Main/>
        </Content>
    </Layout>
    
</div>
        )
    }
}
export default withRouter(NavBar) ;
