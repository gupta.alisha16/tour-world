import React from 'react';
import * as Data from './../dummyData.json';
import { Card, CardTitle, CardActions, Button,  CardText} from 'react-mdl';
import { store } from '../index.js';
import {connect } from 'react-redux';

export class ListOfPlaces extends React.PureComponent {
    constructor(props) {
        super();
    }
    isFavorite = (place) => {
        store.dispatch({
            type : 'PUSH_FAV',
            value : place
        })
    } 

    render(){
        let placeItems="";
        placeItems = Data.Title.map( place => {
            var imgurl = place.imgurl;
            return(
                <Card shadow={5} style={{minWidth: '450', margin: '10px'}}>
                    <CardTitle expand style={{color: '#fff', height: '176px',fontWeight: 'bold', backgroundImage: 'url('+imgurl+')'}}>{place.placeName}</CardTitle>
                    <CardText>{place.desc}</CardText>
                    <CardActions border>
                    <Button colored onClick={()=>{this.isFavorite(place)}} className={place.isFav === "1" ? 'hide' : ''}>Favourite</Button>
                    </CardActions>
                </Card>
            )
        })
        return (
            <div className="project-grid">
                {placeItems}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        favPlaces :  state.fav.favPlaces
    }
}

export default connect(
    mapStateToProps,
)(ListOfPlaces);
