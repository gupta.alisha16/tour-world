import React from 'react';
import { Grid, Cell, Card, CardTitle, CardActions, Button, CardText } from 'react-mdl';
import { connect } from 'react-redux';
import { store } from '../index.js';

export class Favorite extends React.PureComponent {
    constructor(props) {
        super();
    }
    remove = (place) => {
        store.dispatch({
            type: 'REMOVE_FAV',
            value: place
        })
    }
    render() {
        let placeItems = "";
        placeItems = this.props.favPlaces.map(place => {
            var imgurl = place.imgurl;
            return (
                <Card shadow={5} style={{ minWidth: '450', margin: '10px' }}>
                    <CardTitle expand style={{ color: '#fff', height: '176px', fontWeight: 'bold', backgroundImage: 'url(' + imgurl + ')' }}>{place.placeName}</CardTitle>
                    <CardText>{place.desc}</CardText>
                    <CardActions border>
                        <Button colored onClick={() => { this.remove(place) }}>Remove</Button>
                    </CardActions>
                </Card>
            )
        })
        return (
            <div style={{ width: '100%', margin: 'auto' }}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <div className="banner-text">
                            <h1>
                                YOUR FAVOURITES
                           </h1>

                            <hr />
                            <p>
                                Hurrry Up! Travel to your favourite place today.
                           </p>
                        </div>
                    </Cell>
                    <Cell col={12}>
                        {this.props.favPlaces !== null ? <div className="project-grid"> {placeItems} </div> : <div></div>}
                    </Cell>
                </Grid>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        favPlaces: state.fav.favPlaces
    }
}

export default connect(
    mapStateToProps,
)(Favorite);
