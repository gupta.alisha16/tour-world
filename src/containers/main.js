import React from 'react';
import { Switch, Route } from 'react-router-dom';
import LandingPage from './landingPage';
import Favorite from './favorite';

export class Main extends React.PureComponent {
    render () {
        return (
            <div>
                <Switch>
                    <Route exact path="/" component={LandingPage}></Route>
                    <Route path="/favorite" component={Favorite}></Route>
                </Switch>
            </div>
        )
    }
}

export default Main;
