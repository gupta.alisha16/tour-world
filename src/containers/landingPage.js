import React from 'react';
import { Grid, Cell } from 'react-mdl';
import ListOfPlaces from './listOfPlaces';

export class LandingPage extends React.PureComponent {
    render () {
        return (
            <div style={{width: '100%', margin: 'auto'}}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <div className="banner-text">
                           <h1>
                               TOUR THE WORLD
                           </h1>
                           
                           <hr/>
                           <p>
                           It's about the journey | The world is waiting | Life is an adventure | Feed your wanderlust.
                           </p>
                        </div>
                    </Cell>
                    <Cell col={12}>
                        <ListOfPlaces/>
                    </Cell>
                </Grid>
                
            </div>
        )
    }
}
export default LandingPage;
